# vsock-sim介绍

## 背景

qtfs为部署在HOST和DPU上的客户端-服务器模型，二者之间需要进行通信。出于安全性考虑，没有使用开放的网络协议作为qtfs客户端与服务端的通信方式，而是选择vsock这一封闭但是仍具备通用性的协议（用户也可以通过QTFS_TEST_MODE flag编译开启网络模式，建议只作为测试模式）。

由于vsock默认只能用于虚拟化场景，用于宿主机与虚拟机或同宿主机上的虚拟机之间的通信（通过cid区分不同机器），通用环境下无法直接使用vsock，所以vsock-sim就是为了提供一个vsock的仿真环境，用户可以在常规环境中使用vsock协议通信。

未来DPU硬件可根据自定义的通信通道封装为vsock协议对上层应用提供通信。

## 原理

vsock-sim的实现原理为替换了系统的vsock_family_ops，当用户通过socket系统调用创建AF_VSOCK类型socket时，使用vsock-sim提供的socket create接口函数。该函数会创建一个AF_INET类型的socket，并将该socket的部分socket_ops替换为vsock-sim的。主要是其中跟地址相关的ops，如bind、connect等，将这些处理函数的vsock类型地址转换为预先设定好的inet类型地址。

通过上述方式完成vsock类型socket替换为inet类型。

## 使用方式

vsock-sim为内核模块，可以在当前目录下直接编译：

```
# make clean
# make
```

编译完成后生成vsock-sim.ko

通过以下命令插入vsock-sim模块，通过参数指定vsock cid和ip地址之间的对应关系，后续用户对特定vsock cid的使用都会转换为指定ip：

```
# insmod ./vsock-sim.ko cid_ip_map=2-192.168.122.104,3-192.168.122.103
```

上述命令为将cid 2映射到ip 192.168.122.104， 将cid 3映射到ip 192.168.122.103

cid-ip-map参数通`,`分隔cid-ip对；通过`-`分隔cid及ip，左侧为cid，右侧为ip。
