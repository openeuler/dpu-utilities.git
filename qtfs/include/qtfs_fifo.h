#ifndef __QTFS_FIFO_H__
#define __QTFS_FIFO_H__


struct fifo_server_arg_t {
	char *addr;
	unsigned int port;
	unsigned int cid;
	unsigned int family; // AF_VSOCK or AF_INET
};
void *fifo_server_main_thread(void *arg);

#endif

