/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.
 * qtfs licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 * Author: Liqiang
 * Create: 2023-03-20
 * Description: 
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <json-c/json_object.h>
#include <json-c/json_tokener.h>

#include "dirent.h"
#include "rexec.h"

#define rshim_log rexec_log
#define rshim_err rexec_err

void rshim_close_all_fd()
{
	DIR *dir = NULL;
	struct dirent *entry;
	dir = opendir("/proc/self/fd/");
	if (dir == NULL) {
		rshim_err("open path:/proc/self/fd/ failed");
		return;
	}
	while (entry = readdir(dir)) {
		int fd = atoi(entry->d_name);
		if (fd <= 2 || S_ISFIFO(rexec_fd_mode(fd)))
			continue;
		close(fd);
	}
	closedir(dir);
	return;
}

int rshim_get_file_size(char *file)
{
	int size = 0;
	FILE *f = fopen(file, "rb");
	if (f == NULL) {
		rshim_err("File:%s fopen failed.", file);
		return -1;
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fclose(f);
	return size;
}

void rshim_reg_file_open(int fd_target, const char *path, int perm, int offset)
{
	int fd = open(path, perm);
	int fd2 = -1;
	if (fd < 0) {
		rshim_err("Open file:%s failed, fd:%d errno:%d", path, fd, errno);
		return;
	}
	if (fd != fd_target) {
		fd2 = dup2(fd, fd_target);
		if (fd2 != fd_target) {
			rshim_err("Failed to open file:%s by fd:%d", path, fd_target);
			close(fd2);
			close(fd);
			return;
		}
		close(fd);
	}
	int off = lseek(fd_target, offset, SEEK_SET);
	if (off < 0) {
		rshim_err("Failed to set offset:%d to file:%s, fd:%d, fd2:%d", offset, path, fd, fd2);
		return;
	}
	rshim_log("Successed to set offset:%d to file:%s, fd:%d, fd2:%d", offset, path, fd, fd2);
	return;
}

void rshim_reg_file_resume(const char * const json_buf)
{
	struct json_object *obj_files;
	struct json_object *obj_file;
	struct json_object *obj_fd;
	struct json_object *obj_path;
	struct json_object *obj_perm;
	struct json_object *obj_offset;
	int fd, perm, offset;
	const char *path = NULL;
	struct json_object *fd_json = json_tokener_parse(json_buf);
	if (fd_json == NULL) {
		fprintf(stderr, "parse json error\n");
		return;
	}
	obj_files = json_object_object_get(fd_json, "Files");
	int arraylen = json_object_array_length(obj_files);
	for (int i=0; i< arraylen; i++) {
		obj_file = json_object_array_get_idx(obj_files, i);
		obj_fd = json_object_object_get(obj_file, "Fd");
		fd = json_object_get_int(obj_fd);
		obj_path = json_object_object_get(obj_file, "Path");
		path = json_object_get_string(obj_path);
		obj_perm = json_object_object_get(obj_file, "Perm");
		perm = json_object_get_int(obj_perm);
		obj_offset = json_object_object_get(obj_file, "Offset");
		offset = json_object_get_int(obj_offset);
		rshim_log("Get file from json fd:%d path:%s perm:%d offset:%d",
				fd, path, perm, offset);
		rshim_reg_file_open(fd, path, perm, offset);
	}

	json_object_put(fd_json);
	return;
}

int set_rlimit(unsigned int flag, rlim_t value)
{
	char str[32] = {0};
	struct rlimit rlim;
	rlim.rlim_cur = rlim.rlim_max = value;
	if (setrlimit(flag, &rlim) < 0) {
		rexec_err("set rlimit flag:%u failed.", flag);
		return -1;
	}
	rexec_log("set process rlimit flag:%u value:%lu", flag, value);
	return 0;
}

int set_prlimit_handle(unsigned int flag, rlim_t value)
{
	return set_rlimit(flag, value);
}

// {
//	"Attrs": [
//		{"key" : "maxMemLock", "value" : "1048576"},
//		{"key" : "maxProcesses", "value" : "10"},
//		{"key" : "maxFiles", "value" : "1024"},
//		{"key" : "maxCoreSize", "value" : "1048576"},
//	]
// }

struct rexec_pattr_tab {
	unsigned int flag;
	char *key;
	int (*set_value_handler)(unsigned int, rlim_t);
};

// 这里排列要和另一端一模一样
static struct rexec_pattr_tab pattr_table[] = {
	{RLIMIT_MEMLOCK, "maxMemLock", set_prlimit_handle},
	{RLIMIT_NPROC, "maxProcesses", set_prlimit_handle},
	{RLIMIT_NOFILE, "maxFiles", set_prlimit_handle},
	{RLIMIT_CORE, "maxCoreSize", set_prlimit_handle},
};

void rshim_process_attr_resume(const char * const json_buf)
{
	struct json_object *attrs;
	struct json_object *cap;
	struct json_object *key;
	struct json_object *value;
	struct json_object *attr_json = json_tokener_parse(json_buf);
	if (attr_json == NULL) {
		rshim_err("parse json error when get process attr resume");
		return;
	}

	attrs = json_object_object_get(attr_json, "Attrs");
	if (attrs == NULL) {
		rshim_err("get Attrs from json failed, ignore process attributes inherit.");
		goto end;
	}
	int arraylen = json_object_array_length(attrs);
	for (int i=0; i< arraylen; i++) {
		const char *n;
		rlim_t v;
		cap = json_object_array_get_idx(attrs, i);
		if (cap == NULL) {
			rshim_err("get inherit process rlimit index:%d name:%s value:%lu failed.", i, pattr_table[i].key, v);
			goto end;
		}

		key = json_object_object_get(cap, "key");
		n = json_object_get_string(key);
		value = json_object_object_get(cap, "value");
		v = json_object_get_int64(value);

		if (strcmp(n, pattr_table[i].key) != 0) {
			rshim_err("attr inherit json item not match index:%d key:%s jsonkey:%s value:%lu", i, n, pattr_table[i].key, v);
			continue;
		}
		if (pattr_table[i].set_value_handler(pattr_table[i].flag, v) != 0) {
			rshim_err("inherit process rlimit :%s value:%lu failed.", pattr_table[i].key, v);
		} else {
			rshim_log("process attr set success, key:%s value:%lu", n, v);
		}
	}

end:
	json_object_put(attr_json);
	return;
}

/*  
	param list:
	   1) -f xxx.json binary param1 param2 ...
	   2) binary param1 param2...
*/
int rexec_shim_entry(int argc, char *argv[])
{
	char *json_str = NULL;
	char **newarg = NULL;

	if (strcmp(argv[0], "-f") == 0) {
		json_str = argv[1];
		newarg = &argv[2];
	} else {
		newarg = argv;
	}

	rshim_log("Get json str:%s", json_str);
	rshim_close_all_fd();

	rshim_reg_file_resume(json_str);
	rexec_log_son_rebuild();
	rshim_process_attr_resume(json_str);
	execvp(newarg[0], newarg);
	perror("execvp failed.");

	return -1;
}

